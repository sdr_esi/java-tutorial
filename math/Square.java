package math;

public class Square {
   private double side;

   public Square(double side) {
      this.side = side;
   }

   public double getArea() {
      return side * side;
   }

   public double getPerimeter() {
      return 4 * side;
   }

   public String toString() {
      return "Carré de côté " + side;
   }

   public boolean equals(Square that) {
      return this.side == that.side;
   }
}