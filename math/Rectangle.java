package math;

public class Rectangle {
   private double largeur;
   private double longueur;

   public Rectangle(double largeur, double longueur) {
      this.largeur = largeur;
      this.longueur = longueur;
   }

   public double getArea() {
      return largeur * longueur;
   }

   public double getPerimeter() {
      return (largeur + longueur) * 2;
   }
}