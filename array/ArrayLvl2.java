package array;

import util.SODisplay;
import util.ArrayUtil;

public class ArrayLvl2 {
   public static void main(String[] args) {
      SODisplay.printTitle1("Tableau lvl 2");

      dim2Array();
      argsArray(args);
      charArrayAndString();
      System.out.println("-----------------");
      int[][] tab = {{1,2,3},{4,5,6},{7,8,9}};
      ArrayUtil.printArray(tab);
      System.out.println("-----------------");
      tab = rotate(rotate(rotate(rotate(tab))));
      ArrayUtil.printArray(tab);
   }

   public static int[][] rotate(int[][] tab) {
      int size = tab.length;
      int[][] copyTab = new int[size][size];

      for(int i = 0; i < size; i++) {
         for(int j = 0; j < size; j++) {
            copyTab[i][j] = tab[j][size - 1 - i];
         }
      }

      return copyTab;
   }

   private static void dim2Array() {
      SODisplay.printTitle2("Tableau de tableaux");
      /*
       * En java, il est possible de faire des tableaux de tableaux (
       * tableau de dimension 2). Cela est par exemple utilisé pour représenter
       * des matrices (Une image de type .jpg est une image de type matriciel).
       *
       * Un tableau de tableaux d'entiers se définit de la manière suivante :
       */
      SODisplay.printTitle3("Création d'un tableau de dimension 2");
      int[][] tab = new int[10][3];
      ArrayUtil.printArray(tab);

      /*
       * Pour accéder à un élément, à l'instar d'un tableau à une dimension,
       * il suffit d'indiquer les indices de l'élément.
       */
      SODisplay.printTitle3("Accès à l'élément d'indice (0, 0)");
      System.out.println(tab[0][0]);

      /*
       * Même chose si l'on souhaite modifier un élément du tableau.
       */
      SODisplay.printTitle3("Modification de l'élément d'indice (1, 2)");
      tab[1][2] = 3;
      ArrayUtil.printArray(tab);

      /*
       * Remarque, il est possible de faire cela pour tout type de
       * valeurs.
       */
   }

   private static void argsArray(String[] args) {
      /*
       * Dans la méthode main, nous pouvons voir qu'il y a un tableau de String
       * qui lui est passé en paramètre. Celui-ci est complété avec les
       * argument ajouté lors de l'execution du programme.
       *
       * Faisons "java array.ArrayLvl2 1 2 3" pour tester ce programme.
       */
      SODisplay.printTitle2("String[] args");
      SODisplay.printTitle3("Les arguments passés en paramètres sont :");
      ArrayUtil.printArray(args);

      /*
       * Ces paramètres sont de type String. Il est possible de les convertirs
       * en int en utilisant la méthode parseInt de la classe Integer.
       */
      SODisplay.printTitle2("Conversion String -> int");
      int[] tabIntValue = new int[args.length];
      int somme = 0;

      for(int i = 0; i < args.length; i++) {
         somme += Integer.parseInt(args[i]);
      }

      System.out.println("La somme des entrées en paramètre est : " + somme);

      /*
       * Il est possible de faire cette conversion vers d'autres type (voir API
       * selon le type souhaité).
       */
   }

   private static void charArrayAndString() {
      SODisplay.printTitle2("Conversion de String -> char[] et inversement");
      /*
       * Une chaine de caractère (String) est un ensemble de caractères stockés
       * sous forme d'un tableau de caractère (char[]). Il est possible de
       * récupérer une copie de ce tableau.
       */
      SODisplay.printTitle3("Conversion de la chaîne \"Bonjour\" & affichage");
      String str = "Bonjour";
      char[] charArray= str.toCharArray();
      ArrayUtil.printArray(charArray);

      /*
       * Modifion le string, pour chaque caractère, on prend celui qui vient
       * après dans l'alphabet.
       */
      for(int i = 0; i < charArray.length; i++) {
         charArray[i] = (char) ((int) charArray[i] + 1);
      }

      /*
       * On convertit le tableau en une chaîne de caractères String.
       */
      SODisplay.printTitle3("La nouvelle chaîne de caractères est");
      String strModif = new String(charArray);
      System.out.println(strModif);
   }
}