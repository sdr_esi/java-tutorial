package array;

import util.ArrayUtil;
import util.SODisplay;

import java.util.Scanner;

/**
 * Cette classe contient des exemples visant à introduire l'utilisation des
 * tableaux, i. e. , la déclaration, l'initialisation, son comportement par
 * référence ainsi que les erreurs les plus couramment rencontrée lors de
 * L'utilisation de tableau.
 */
public class ArrayLvl1 {
   public static void main(String[] args) {
      SODisplay.printTitle1("Introduction aux tableaux");
      arrayCreation();
      arrayValueModification();
      arrayReferenceType();
   }

   /*
      Dans cette section, nous voyons comment créer un tableau et quelles
      valeurs sont données à leurs éléments.
    */
   public static void arrayCreation() {
      SODisplay.printTitle2("Création d'un tableau");
      /*
         Les moments où le nombre de variables à utiliser n'est pas connu sont
         nombreuse. Dans ce cas, on peut utiliser un tableau (d'autre structure
         de données telles que les files, les tas, les arbres... sont
         préférables selon les cas, mais celles-ci seront vues plus tard).
         Ainsi, au lieu d'utiliser 6 variables entières telle que ci dessous,
         nous pouvons utiliser un tableau de taille 6.
       */

      int variable1 = 0;
      int variable2 = 0;
      int variable3 = 0;
      int variable4 = 0;
      int variable5 = 0;
      int variable6 = 0;

      /*
         On créé un tableau d'entiers de taille 6.
       */
      int[] array = new int[6];

      /*
        On affiche son contenu à l'écran. Les éléments de celui-ci sont
        initialisés à 0 par défaut. La méthode printArray() de ArrayUtil est
        utilisée pour faire cela.
       */
      SODisplay.printTitle3("Affichage du tableau.");
      ArrayUtil.printArray(array);

      /*
         Les tableaux peuvent contenir d'autres données que les entiers. Ils
         Peuvent contenir des primitifs (doubles, des floats...) mais
         également des objets (Scanner, String, Integer, Double...). Il est à
         noter à ce stade que les tableaux sont égalements un type de donnée
         primitif. Il est donc possible de créer un tableau qui contient des
         tableaux.

         De manière général, les éléments d'un tableau sont initialisés à :
         0     -> pour un tableau de numériques primitifs.
         false -> pour un tableau de booléen primitifs.
         null  -> pour les tableaux d'objets (pas initialisés).
       */
      SODisplay.printTitle3("Les tableaux sont initialisés différements " +
              "selon les types.");
      System.out.print("Un tableau de double : ");
      ArrayUtil.printArray(new double[1]);
      System.out.print("Un tableau de float : ");
      ArrayUtil.printArray(new float[1]);
      System.out.print("Un tableau de byte : ");
      ArrayUtil.printArray(new byte[1]);
      System.out.print("Un tableau de char : ");
      ArrayUtil.printArray(new char[1], '\''); // élément ASCII n° 0
      System.out.print("Un tableau de tableaux d'entiers : ");
      ArrayUtil.printArray(new int[1][1]);
      System.out.print("Un tableau de String : ");
      ArrayUtil.printArray(new String[1]);
      System.out.print("Un tableau de Scanner : ");
      ArrayUtil.printArray(new Scanner[1]);
      System.out.print("Un tableau de Integer : ");
      ArrayUtil.printArray(new Integer[1]);
      System.out.print("Un tableau de Double : ");
      ArrayUtil.printArray(new Double[1]);

      //En java, à l'exception des types primitifs, tout est objet !
      System.out.print("Un tableau d'Objet : ");
      ArrayUtil.printArray(new Object[1]);
   }

   public static void arrayValueModification() {
      SODisplay.printTitle2("Accès et modification aux éléments d'un tableau");

      /*
         Il est possible de créer un tableau en lui attribuant directement
         des valeurs à ses éléments. Prenons l'exemple d'un tableau d'entiers.

         Remarque : Même si la notation d'un ensemble est utilisée ({}),
         celle-ci diffère de la notion mathématique d'un ensemble. En effet,
         un élément peut se retrouver plusieurs fois dans un tableau. Cela n'est
         pas vrai dans un ensemble mathématique.
       */
      SODisplay.printTitle3("Attribution de valeurs lors de la création");
      int[] array = {7, 8, 9, 10, 11, 11};
      ArrayUtil.printArray(array);

      /*
         Cette manière de procéder ne peut se faire que lors de la déclaration
         du tableau. Elle ne peut se faire après. Ainsi, le code suivant n'est
         pas correct :
         | int[] array2;
         | array2 = {1, 2, 3};
         Il est cependant possible de lui donner l'ensemble de ses valeurs de
         la manière suivante :
       */
      int[] array2;
      array2 = new int[]{1, 2, 3};
      ArrayUtil.printArray(array2);

      /*
         En considérant taille(tableau) comme étant la taille du tableau,
         les indices d'un tableau font partie de
         l'ensemble [0, taille(tableau)[.
         Pour modifier un élément d'un tableau, il faut donner l'indice
         de l'élément à modifier et ainsi lui assigner une valeur. Les
         instructions suivantes réalisent, respectivement, les actions:
         le premier éléments de array prend la valeur 6;
         le deuxième éléments de array prend la valeur 7;
         le troisième éléments de array prend la valeur du premier élément;
       */
      array[0] = 6;
      array[1] = 7;
      array[2] = array[0];
      array[0] = 5;

      SODisplay.printTitle3("Les 3 premiers éléments du tableau ont été" +
                            "modifiés.");
      ArrayUtil.printArray(array);

      /*
         Un tableau connaît sa taille via sa variable length.
       */
      SODisplay.printTitle3("On affiche la longueur du tableau.");
      System.out.println(array.length);

      /*
         Ainsi, l'instruction suivante donne la valeur 42 au
         dernier élément du tableau.
       */
      array[array.length - 1] = 42;
      SODisplay.printTitle3("Le dernier élément du tableau prend la valeur 42");
      ArrayUtil.printArray(array);

      /*
         On peut accéder à un élément de ce tableau en faisant array[index].
         index allant de [0, array.length), i.e., 0 <= index < array.length.
         En lecture :
       */
      SODisplay.printTitle3("On boucle pour lister les éléments du tableau.");
      for(int i = 0; i < array.length; i++) {
         System.out.println(array[i]);
      }

      /*
         En écriture :
       */
      SODisplay.printTitle3("Modification du tableau -> [0, 1, 2, 3, 4, 5]");
      for(int i = 0; i < array.length; i++) {
         array[i] = i;
      }

      ArrayUtil.printArray(array);
   }

   private static void arrayReferenceType() {
      int[] array = new int[6];

      /*
         Lorsque l'on passe un tableau en paramètre, les éléments de celui-ci
         peuvent être modifiés. Ici on modifie l'élément d'indice 2 avec la
         méthode modifElement(..)
       */
      SODisplay.printTitle3("On modifie la valeur du 3ème élément via " +
              " une méthode.");
      modifElement(array, 2, 42);
      // Imprimons le tableau pour voir si le troisème élément a bien été
      // modifié.
      ArrayUtil.printArray(array);

      /*
         Si l'on imprime le tableau dans la ST, nous obtenons une référence.
       */
      SODisplay.printTitle3("Impression de array");
      System.out.println(array);
      /*
         Cette référence est en réalité un lien vers l'emplacement mémoire du
         tableau. Lorsqu'elle est passée en paramètre à une fonction, cette
         référence est copiée. La référence étant la même, ceci explique le fait
         que nous pouvons modifier l'élément d'un tableau au sein d'une méthode
         Cependant, si nous modifions la référence du tableau, il n'y a que la
         copie qui est altérée. Voir le changement de référence après
         modification, dans et hors de la méthode.
       */
      SODisplay.printTitle3("Modification d'un tableau dans une méthode.");
      arrayModification(array);
      System.out.println("Référence hors de la méthode après modification :");
      System.out.println(array);
   }

   public static void arrayError() {
      /*
         Certaines exception sont couramment lancées lorsqu’on utilise des
         tableaux. Celle-ci sont les très connues NullPointerException ainsi que
         ArrayIndexOutOfBoundsException.

         NullPointerException intervient lorsqu'on essaie d'utiliser un élémment
         d'un tableau d'objet qui n'est pas encore initialisé.
       */

      try {
         Scanner[] scannerArray = new Scanner[1];
         scannerArray[0].nextInt();
      } catch(NullPointerException e) {
         System.out.println("L'exception NullPointerException a été lancée !");
      }
      /*
       * ArrayIndexoutOfBoundsException survient lorsqu'on essaie d'accéder à un
       * élément qui n'existe pas. Si le tableau a une taille de n, l'accès aux
       * indices compris dans l'ensemble ]-infini, -1] ainsi que [n, infini]
       * provoquera cette exception.
       */
      try {
         int n = 1;
         Scanner[] scannerArray = new Scanner[n];
         scannerArray[n].nextInt();
      } catch(ArrayIndexOutOfBoundsException e) {
         System.out.println("L'exception ArrayIndexOutOfBoundsException a " +
                 "été lancée (accès à l'élément d'indice n !");
      }
      try {
         int n = 1;
         Scanner[] scannerArray = new Scanner[n];
         scannerArray[-1].nextInt();
      } catch(ArrayIndexOutOfBoundsException e) {
         System.out.println("L'exception ArrayIndexOutOfBoundsException a " +
                 "été lancée (accès à l'élément d'indice -1 !");
      }
   }

   /**
    * Cette méthode permet d'attribuer une valeur à un élément d'un tableau.
    * @param array Le tableau qui doit être modifié.
    * @param index L'indice de l'élément à modifier.
    * @param value La valeur à attribuer.
    */
   public static void modifElement(int[] array, int index, int value) {
      array[index] = value;
   }

   /**
    * Cette méthode modifie le tableau passé en paramètre.
    * @param array Le tableau modifié.
    */
   public static void arrayModification(int[] array) {
      System.out.println("Référence dans la méthode avant modification :");
      System.out.println(array); // La référence est bien la même.
      array = new int[10];
      System.out.println("Référence dans la méthode après modification :");
      System.out.println(array); // La référence est modifiée.
   }
}