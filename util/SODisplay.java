package util;

/**
 * Cette classe fournit des outils de mise en forme dans la sortie standard.
 */
public class SODisplay {

   public static void printTitle1(String message) {
      printTitle(message, '*', 3, 1, 3, 2, 5);
   }

   public static void printTitle2(String message) {
      printTitle(message, '*', 2, 1, 1, 1, 3);
   }

   public static void printTitle3(String message) {
      printTitle(message, '-', '|', 1, 0, 1, 0, 1);
   }

   private static void printTitle(String message, char character,
                                  int nbSpaceLineAv, int nbSpaceLineAp,
                                  int nbSeparator, int nbInnerSpaceLine,
                                  int nbSpace) {
      printTitle(message, character, character,
                 nbSpaceLineAv, nbSpaceLineAp,
                 nbSeparator, nbInnerSpaceLine,
                 nbSpace);
   }

   private static void printTitle(String message,
                                  char character, char character2,
                                  int nbSpaceLineAv, int nbSpaceLineAp,
                                  int nbSeparator, int nbInnerSpaceLine,
                                  int nbSpace) {

      int size = message.length() + (nbSpace + nbSeparator) * 2;

      printSpaceLine(nbSpaceLineAv);

      printLineSeparator(character, size, nbSeparator);

      printInnerSpaceLine(nbInnerSpaceLine, character2, nbSeparator, size);

      printInnerLine(character2, nbSeparator, nbSpace, message);

      printInnerSpaceLine(nbInnerSpaceLine, character2, nbSeparator, size);

      printLineSeparator(character, size, nbSeparator);

      printSpaceLine(nbSpaceLineAp);

   }

   private static void printSpaceLine(int nbr) {
      for(int i = 0; i < nbr; i++) {
         System.out.println();
      }
   }

   private static void printLineSeparator(char character, int size, int nbr) {
      String separator = getSeparator(character, size);

      for(int i = 0; i < nbr; i++) {
         System.out.println(separator);
      }
   }

   private static void printInnerSpaceLine(int nbLine, char character,
                                           int nbCharacter, int size) {
      for(int i = 0; i < nbLine; i++) {
         printInnerSpaceLine(character, nbCharacter, size);
      }
   }

   private static void printInnerSpaceLine(char character, int nbCharacter,
                                          int size) {
      int nbSpaces = size - nbCharacter * 2;

      char[] line = new char[size];

      // On remplit la ligne d'espace.
      for(int i = 0; i < size; i++) {
         line[i] = ' ';
      }

      // On fait le cadre
      for(int i = 0; i < nbCharacter; i++) {
         line[i] = character;
         line[size -1 - i] = character;
      }

      System.out.println(new String(line));
   }

   private static void printInnerLine(char character, int nbCharacter,
                                     int nbSpaces, String message) {
      int size = message.length() + 2 * (nbCharacter + nbSpaces);

      char[] line = new char[size];

      // On remplit la ligne d'espace.
      for(int i = 0; i < size; i++) {
         line[i] = ' ';
      }

      // Création du cadre.
      for(int i = 0; i < nbCharacter; i++) {
         line[i] = character;
         line[size - 1 - i] = character;
      }

      // Copie du message.
      int margin = nbCharacter + nbSpaces;
      for (int i = 0; i < message.length(); i++) {
         line[margin + i] = message.charAt(i);
      }

      System.out.println(line);
   }

   private static String getSeparator(char character, int size) {
      char[] separator = new char[size];

      for(int i = 0; i < separator.length; i++) {
         separator[i] = character;
      }

      return new String(separator);
   }
}