package util;

/**
 * Cette classe fournit des outils pour visualiser des tableaux.
 */
public class ArrayUtil {
   /**
    * Cette méthode permet d'imprimer sur la sortie standard
    * les éléments d'un tableau d'entiers.
    * @param array Le tableau d'entier.
    */
   public static void printArray(int[] array) {
      printArray(array, (char) 0);
   }

   /**
    * Cette méthode permet d'imprimer sur la sortie standard
    * les éléments d'un tableau d'entiers.
    * @param array Le tableau d'entier.
    * @param embellisher Un caractère pour entourer les éléments du tableau.
    */
   public static void printArray(int[] array, char embellisher) {
      for(int i = 0; i < array.length; i++) {
         System.out.println("" + embellisher + array[i] + embellisher);
      }
   }

   /**
    * Cette méthode permet d'imprimer sur la sortie standard
    * les éléments d'un tableau de double.
    * @param array Le tableau de double.
    */
   public static void printArray(double[] array) {
      printArray(array, (char) 0);
   }

   /**
    * Cette méthode permet d'imprimer sur la sortie standard
    * les éléments d'un tableau de double.
    * @param array Le tableau de double.
    * @param embellisher Un caractère pour entourer les éléments du tableau.
    */
   public static void printArray(double[] array, char embellisher) {
      for(int i = 0; i < array.length; i++) {
         System.out.println("" + embellisher + array[i] + embellisher);
      }
   }
     

   /**
    * Cette méthode permet d'imprimer sur la sortie standard
    * les éléments d'un tableau de float.
    * @param array Le tableau de float.
    */
   public static void printArray(float[] array) {
      printArray(array, (char) 0);
   }

   /**
    * Cette méthode permet d'imprimer sur la sortie standard
    * les éléments d'un tableau de float.
    * @param array Le tableau de float.
    * @param embellisher Un caractère pour entourer les éléments du tableau.
    */
   public static void printArray(float[] array, char embellisher) {
      for(int i = 0; i < array.length; i++) {
         System.out.println("" + embellisher + array[i] + embellisher);
      }
   }

   /**
    * Cette méthode permet d'imprimer sur la sortie standard
    * les éléments d'un tableau de char.
    * @param array Le tableau de char.
    */
   public static void printArray(char[] array) {
      printArray(array, (char) 0);
   }

   /**
    * Cette méthode permet d'imprimer sur la sortie standard
    * les éléments d'un tableau de char.
    * @param array Le tableau de char.
    * @param embellisher Un caractère pour entourer les éléments du tableau.
    */
   public static void printArray(char[] array, char embellisher) {
      for(int i = 0; i < array.length; i++) {
         System.out.println("" + embellisher + array[i] + embellisher);
      }
   }

   /**
    * Cette méthode permet d'imprimer sur la sortie standard
    * les éléments d'un tableau de byte.
    * @param array Le tableau de byte.
    */
   public static void printArray(byte[] array) {
      printArray(array, (char) 0);
   }

   /**
    * Cette méthode permet d'imprimer sur la sortie standard
    * les éléments d'un tableau de byte.
    * @param array Le tableau de byte.
    * @param embellisher Un caractère pour entourer les éléments du tableau.
    */
   public static void printArray(byte[] array, char embellisher) {
      for(int i = 0; i < array.length; i++) {
         System.out.println("" + embellisher + array[i] + embellisher);
      }
   }

   /**
    * Cette méthode permet d'imprimer sur la sortie standard
    * les éléments d'un tableau d'Object.
    * @param array Le tableau d'Object.
    */
   public static void printArray(Object[] array) {
      printArray(array, (char) 0);
   }

   /**
    * Cette méthode permet d'imprimer sur la sortie standard
    * les éléments d'un tableau d'Object.
    * @param array Le tableau d'Object.
    * @param embellisher Un caractère pour entourer les éléments du tableau.
    */
   public static void printArray(Object[] array, char embellisher) {
      for(int i = 0; i < array.length; i++) {
         System.out.println("" + embellisher + array[i] + embellisher);
      }
   }

   /**
    * Cette méthode permet d'imprimer sur la sortie standard les éléments
    * d'un tableau d'entier à deux dimensions.
    * @param array Le tableau d'entiers de dimension 2.
    */
   public static void printArray(int[][] array) {
      for(int i = 0; i < array[0].length; i++) {
         for(int j = 0; j < array.length; j++) {

            System.out.printf("%3d", array[j][i]);
         }

         System.out.println();
      }
   }

   /**
    * Cette méthode permet de mettre tout le tableau à 0.
    * @param array Le tableau à reset.
    */
   public static void resetArray(int[] array) {
      for(int i = 0; i < array.length; i++) {
         array[i] = 0;
      }
   }
}