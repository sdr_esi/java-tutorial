package questions.util;

public class Messages {
    public static void congratulation() {
        System.out.println("Bien joué !\n");
        System.out.println("                                ______");
        System.out.println("                           _,-\"\"    __\'\"\"==,,_");
        System.out.println("                         ,\"    _-\'\'   \'\'>-_   \"V-<=_");
        System.out.println("                        /          _.-=_   `  >< `  \",");
        System.out.println("                      ,\"        ,-\'     ,<\"/\'\"  \"=,`. \\");
        System.out.println("                     /     /  .\"      .\"  (       \\\\ . )");
        System.out.println("                    /   / /       _>\"\'    |        )\\` |");
        System.out.println("                   /   / /      /|\'       )        | ) )");
        System.out.println("                  //  /     ,  ) )       /           | ,");
        System.out.println("                 >         )  / /                    |/");
        System.out.println("                 |           (    _=***=,            |(");
        System.out.println("                 |     ,-, \\ |   \'      \"=,          ]");
        System.out.println("                 (   //   |=||      ___    \"    _,=**>");
        System.out.println("                  \\  / /  |V )     _   `         __ .\'");
        System.out.println("                  | /| ( (  |/\"=    \"***\"     ,<C) 7\'");
        System.out.println("                  \\  \\  ` | |\'  \"=    _.._     `\'\"\'f");
        System.out.println("                  /   \\     (     \"=,\"    \", |  _..L");
        System.out.println("                 //^\\  \\_/  |       (      )=-,\"    \",");
        System.out.println("                /  / ) |    /       `=,__./  |(      )");
        System.out.println("                |     )|                   \"\" `=,__./");
        System.out.println("                 \\      \\            -.___ __   /");
        System.out.println("                  \\_     -.          \\  -_\"_ ) /");
        System.out.println("                 /        \\\"-,        \"._   / /");
        System.out.println("                |        \\|   \"-,        \'\"\" < __");
        System.out.println("            _.-\"\\         \\\\     \"-,        \'    \'\'-.");
        System.out.println("        _-<\"\\    \\//`       \\       \'-.___-\'         \'.");
        System.out.println("     .=\'     \\   /          |          /.   \\          \\");
        System.out.println("   ,\"         \\ |       \\\\  |          | \\   \\          .");
        System.out.println("  /            .|        \\\\/           |  ,   ,         |");
        System.out.println(" /             ||          \\           |   .   .        |");
        System.out.println(",              | \\          \\          \\   |   |        |");
        System.out.println("|              |  \\//       |              |   |        |");
    }

    public static void problem1() {
        System.out.println("Je pense que tu devrais aller relire ton cours !\n");
        System.out.println("                .-~~~~~~~~~-._       _.-~~~~~~~~~-.");
        System.out.println("            __.\'              ~.   .~              `.__");
        System.out.println("          .\'//                  \\./                  \\\\`.");
        System.out.println("        .\'//                     |                     \\\\`.");
        System.out.println("      .\'// .-~\"\"\"\"\"\"\"~~~~-._     |     _,-~~~~\"\"\"\"\"\"\"~-. \\\\`.");
        System.out.println("    .\'//.-\"                 `-.  |  .-\'                 \"-.\\\\`.");
        System.out.println("  .\'//______.============-..   \\ | /   ..-============.______\\\\`.");
        System.out.println(".\'______________________________\\|/______________________________`.");
    }

    public static void easterEgg() {
        System.out.println("                              !         !");
        System.out.println("                             ! !       ! !");
        System.out.println("                            ! . !     ! . !");
        System.out.println("                               ^^^^^^^^^ ^");
        System.out.println("                             ^             ^");
        System.out.println("                           ^  (0)       (0)  ^");
        System.out.println("                          ^        \"\"         ^");
        System.out.println("                         ^   ***************    ^");
        System.out.println("                       ^   *                 *   ^");
        System.out.println("                      ^   *   /\\   /\\   /\\    *    ^");
        System.out.println("                     ^   *                     *    ^");
        System.out.println("                    ^   *   /\\   /\\   /\\   /\\   *    ^");
        System.out.println("                   ^   *                         *    ^");
        System.out.println("                   ^  *                           *   ^");
        System.out.println("                   ^  *                           *   ^");
        System.out.println("                    ^ *                           *  ^");
        System.out.println("                     ^*                           * ^");
        System.out.println("                      ^ *                        * ^");
        System.out.println("                      ^  *                      *  ^");
        System.out.println("                        ^  *       ) (         * ^");
        System.out.println("                            ^^^^^^^^ ^^^^^^^^^");
    }

    public static void problem2() {
        System.out.println(",----------------------------------------------------,");
        System.out.println("| [][][][][]  [][][][][]  [][][][]  [][__]  [][][][] |");
        System.out.println("|                                                    |");
        System.out.println("|  [][][][][][][][][][][][][][_]    [][][]  [][][][] |");
        System.out.println("|  [_][][][][][][][][][][][][][ |   [][][]  [][][][] |");
        System.out.println("| [][_][][][][][][][][][][][][]||     []    [][][][] |");
        System.out.println("| [__][][][][][][][][][][][][__]    [][][]  [][][]|| |");
        System.out.println("|   [__][________________][__]              [__][]|| |");
        System.out.println("`----------------------------------------------------\'");
        System.out.println("Un problème avec ton clavier ?");
    }

    public static void seriously2() {
        System.out.println("                      .  !\\  _");
        System.out.println("                      l\\/ ( /(_");
        System.out.println("                    _ \\`--\" _/ .");
        System.out.println("                     \\~\")   (_,/)");
        System.out.println("                     _)/.   ,\\,/");
        System.out.println("            _____,-\"~    \\ /   \"~\"-._____");
        System.out.println("        ,-~\"     \"~-.  .  \"  .  ,-~\"     \"~-.");
        System.out.println("      ,^             ^. `. .\' ,^             ^.");
        System.out.println("     /                 \\  ^  /                 \\");
        System.out.println("    Y___________________Y   Y___________________Y");
        System.out.println("    | |^~\"|^   _ ^|\"~^| |   | |\"~\"|^ _   ^|\"~\"| |");
        System.out.println("    | !   l   (_) !   ! l   | !   l (_)   !   ! |");
        System.out.println("    l  \\  `\\.___,/\'  /  !   l  \\  `\\.___,/\'  /  !");
        System.out.println("     \\  ^.         ,^  /!   !\\  ^.         ,^  /");
        System.out.println("      ^.  ~-------~  ,^\\`v-v\'/^.  ~-------~  ,^");
        System.out.println("      _)~-._______,-~   }---{   ~-._______,-~(_");
        System.out.println(" .--\"~           ,-^7\' /     \\ `Y^-,           ~\"--.");
        System.out.println("/               (_,/ ,/\'     `\\. \\._)    ___        \\");
        System.out.println("\\_____.,--\"~~~\"--..,__        ___,..--<\"~   ~\"-.,___/");
        System.out.println("    / (    __,--~ _.._\"\"~~~~\"\" ,-\"  \"-.`\\     /~.-\"");
        System.out.println("    `._\"--~_,.--\"~    \\       /        \\ `---\' /");
        System.out.println("       \"~\"\"            \\     /          \"-.__,/");
        System.out.println("                       `L   ]\'");
        System.out.println("                        l   !");
        System.out.println("                        j___L");
        System.out.println("                       (_____)");
        System.out.println("                        |   |");
    }
}