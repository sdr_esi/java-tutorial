package questions.tester;

import questions.math.Figure;
import questions.math.Table;
import java.util.Scanner;

public class Q1 {

    public static void mainQuestion1(Scanner clavier) {
        System.out.print("Veuillez entrer le caractère à utiliser pour " +
                "dessiner le losange : ");
        char caractereLosange = clavier.next().charAt(0);

        System.out.print("Veuillez entrer la taille du côté du losange [1, 42): ");
        int coteLosange = clavier.nextInt();

        try {
            Figure.printLosange(caractereLosange, coteLosange);
        } catch(IllegalArgumentException e) {
            System.out.println("La taille passée en paramètre doit appartenir " +
                    "à l'ensemble [1, 42)");
        }
    }

    public static void mainQuestion2(Scanner clavier) {
        System.out.print("Veuillez entrer le nombre : ");
        int nombre = clavier.nextInt();
        System.out.println("Entre les 10 premiers multiples de " + nombre + ".");
        Table.tableTester(clavier, nombre);
    }

    public static void main(String[] args) {
        Scanner clavier = new Scanner(System.in);
        if(args.length == 0) {
            mainQuestion1(clavier);
            mainQuestion2(clavier);
        } else {
            switch(Integer.parseInt(args[0])) {
                case 1:
                    mainQuestion1(clavier);
                    break;
                case 2:
                    mainQuestion2(clavier);
                    break;
                default:
                    System.out.println("1:Question1; 2:Question2");
                    break;
            }
        }
    }
}