package questions.math;

import questions.util.Messages;

/**
 * Cette classe permet d'imprimer des figures de tailles quelconque telles que
 * des carrés, des losanges... sur la sortie standard. Pour l'instant,
 * uniquement l'impression des losanges est implémentée.
 * @author sdr
 */
public class Figure {

    /**
     * Cette méthode est utilisée pour imprimer, sur la sortie standard,
     * un losange de taille et de caractères variables (avec easter egg).
     * @param caractere Le caractère à utiliser pour imprimer le losange.
     * @param cote La longueur du cote du losange. Cette longueur doit être plus
     *             grande que 0 et plus petite que 41. Si la longueur est 42,
     *             un easter egg se produit.
     * @throws IllegalArgumentException Lorsque cote &lt; 1 ou cote &gt; 42.
     */
    public static void printLosange(char caractere, int cote) {
        if(cote < 1 || cote > 42) {
            throw new IllegalArgumentException();

        } if(cote == 42) {
            Messages.easterEgg();

        } else {
            printLosangeSans(caractere, cote);
        }
    }

    /**
     * Cette méthode est utilisée pour imprimer, sur la sortie standard,
     * un losange de taille et de caractères variables (sans easter egg).
     * @param caractere Le caractere utilisé pour dessiner le losange.
     * @param cote La taille du côté du losange.
     */
    public static void printLosangeSans(char caractere, int cote) {
      /*
         On imprime la partie supérieur du losange.
         Si cote = 4 :       ...*      (nbSpaces = 3; nbCaracters = 1)
                             ..***     (nbSpaces = 2; nbCaracters = 3)
                             .*****    (nbSpaces = 1; nbCaracters = 5)
       */
        int nbSpaces = cote - 1;
        int nbCaracteres = 1;

        for(int i = 0; i < cote - 1; i++) { // attention, ne pas mettre nbSpaces au lieu de cote - 1
            printLigne(nbSpaces, nbCaracteres, caractere);

            nbSpaces--;
            nbCaracteres += 2;
        }

      /*
                             *******   (nbSpaces = 0; nbCaracters = 7)
       */
        //printLigne(nbSpaces, nbCaracteres, caractere);
        System.out.println(getLigne(nbSpaces, nbCaracteres, caractere));

      /*
         On imprime la partie supérieur du losange.
         Si cote = 4 :       .*****    (nbSpaces = 1; nbCaracters = 5)
                             ..***     (nbSpaces = 2; nbCaracters = 3)
                             ...*      (nbSpaces = 3; nbCaracters = 1)
         Par rapport à l'exemple, nous aurions actuellement :
          (nbSpaces = -1; nbCaractère = 9). Il faut donc réajuster ces nombres !
       */

        for(int i = 0; i < cote - 1; i++) {
            nbSpaces++;
            nbCaracteres -= 2;

            printLigne(nbSpaces, nbCaracteres, caractere);
        }
    }

    /**
     * Cette méthode retourne une chaîne de caractères avec un nombre donné de
     * de fois un caratère.
     * @param caractere Le caractère à recopier plusieurs fois.
     * @param nbCaracteres Le nombre de caractères à avoir dans la chaîne.
     * @return La chaîne de caractères avec le nombre donné de caractères.
     */
    public static String getLigne(char caractere, int nbCaracteres) {
        String ligne = "";

        for(int i = 0; i < nbCaracteres; i++) {
            ligne += caractere;
        }

        return ligne;
    }

    /**
     * Cette méthode retourne une chaîne de caractères avec un nombre donné de
     * de fois un caratère précédé par un nombre donné d'espaces.
     * @param nbEspaces Le nombre d'espace devant précéder la ligne de caractères.
     * @param nbCaracteres Le nombre de caractères à avoir dans la chaîne.
     * @param caractere Le caractère à recopier plusieurs fois.
     * @return La chaîne de caractères avec le nombre donné de caractères.
     */
    public static String getLigne(int nbEspaces, int nbCaracteres, char caractere) {
        return getLigne(' ', nbEspaces) +
                getLigne(caractere, nbCaracteres);
    }

    /**
     * Cette méthode imprime une chaîne de caractère avec un nombre donné de
     * caractère précédé par un nombre donné d'espaces.
     * @param nbEspaces Le nombre d'espace devant précéder la ligne de caractères.
     * @param nbCaracteres Le nombre de caractères à avoir dans la chaîne.
     * @param caractere Le caractère à recopier plusieurs fois.
     */
    public static void printLigne(int nbEspaces, int nbCaracteres, char caractere) {
        String ligne = getLigne(nbEspaces, nbCaracteres, caractere);

        System.out.println(ligne);
    }
}