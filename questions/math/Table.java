package questions.math;

import questions.util.Messages;
import java.util.Scanner;

/**
 * Cette classe permet de tester les connaissances des tables de multiplication
 * de l'utilisateur.
 * @author sdr
 */
public class Table {
    /**
     * Cette méthode permet de tester si un utilisateur connait sa table de
     * multiplication d'un nombre donné.
     * @param clavier Un lecteur sur l'entrée standard.
     * @param nombre Le nombre concerné.
     */
    public static void tableTester(Scanner clavier, int nombre) {
        int limite = 10;
        int sommeNbFautes = 0;

        for(int i = 1; i <= limite; i++) {
            sommeNbFautes += testMult(clavier, i, nombre);
        }

        System.out.println("\nTu as fait " + sommeNbFautes + " faute(s)");

        if(sommeNbFautes == 0) {
            Messages.congratulation();

        } else if(sommeNbFautes >= 3) {
            Messages.problem1();
        }
    }

    /**
     * Cette méthode demande à l'utilisateur le multiple entre deux nombre.
     * S'il ne trouve pas la réponse, elle lui est directement donnée. Le nombre
     * d'erreur est donc de 0 ou 1.
     * @param clavier Le scanner sur lequel la demande doit être faite.
     * @param f1 Le premier facteur de la multiplication.
     * @param f2 le deuxième facteur de la multiplication.
     * @return Le nombre d'erreurs commises (max 1).
     */
    public static int testMult(Scanner clavier, int f1, int f2) {
        int reponse = f1 * f2;

        System.out.print(multString(f1, f2));

        if(clavier.nextInt() != reponse) {
            System.out.println("La bonne réponse est " + multString(f1, f2, reponse));
            return 1;
        }

        return 0;
    }

    /**
     * Cette méthode permet d'imprimer une chaine de caractère représentant la
     * multiplication de deux nombres sous la forme "f1 × f2 = ". Ainsi, si
     * f1 = 4 et f2 = 5, la chaîne de caractère retournée sera "4 × 5 = ".
     * @param f1 Le premier facteur de la multiplication.
     * @param f2 Le deuxième facteur de la multiplication.
     * @return La chaîne de caractère qui est sous la forme "f1 × f2 =".
     */
    public static String multString(int f1, int f2) {
        return "" + f1 + " × " + f2 + " = ";
    }

    /**
     * Cette méthode permet d'imprimer une chaine de caractère représentant la
     * multiplication de deux nombres sous la forme "f1 × f2 = reponse". Ainsi,
     * si f1 = 4, f2 = 5 et reponse = 20, la chaîne de caractère retournée sera
     * "4 × 5 = 20".
     * @param f1 Le premier facteur de la multiplication.
     * @param f2 Le deuxième facteur de la multiplication.
     * @param reponse La réponse à la multiplication.
     * @return La chaîne de caractère qui est sous la forme "f1 × f2 = reponse".
     */
    public static String multString(int f1, int f2, int reponse) {
        return multString(f1, f2) + reponse;
    }
}
