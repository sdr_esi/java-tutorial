package variable;

import java.util.Scanner;

import util.ArrayUtil;
import util.SODisplay;
import math.*;

public class VariablesLvl2 {
   public static void main(String[] args) {
      declarationEtInitialisation();

      // Convention de nommage
      conventionDeNomage();

      // Notion de bloc
      bloc();

      // Portée des variables
      portee();

      allocationMemoire();
   }

   private static void declarationEtInitialisation() {
      /*
         Déclaration et initialisation de variables

         <type> <identifier> [= expression]

         Les conventions de nommage pour l'identifier sont :
          - utilisation du style mixedCase (première lettre minuscule, première lettre d'un nouveau nom en maj;
          - utilisation possible des caractères :
               ° javaletter
               ° $
               ° _
               ° numéro (jamais le premier caractère sinon erreur à la compil.)
       */
      int a;         // déclaration de la variable entière a
      a = 5;         // a <- 5
      int b = 4;     // déclaration de la variable entière b et b <- 4
      int c = a;     // déclaration de la variable entière c et c <- a (a = 5 => c <- 5)
      a = 3;         // modification de la valeur de a
      System.out.println("a = " + a + "; b = " + b + "; c = " + c);
      int b$ = 41;
      System.out.println("b$ = " + b$);
      int $b = 43;
      System.out.println("$b = " + $b);
      int b1 = 32;
      System.out.println("1b = " + b1);
      // Décomenter les deux prochaines lignes pour tester l'erreur.
      //int 1b = 23;
      //System.out.println("1b = " + 1b);

      //décommenter les 2 prochaines lignes pour tester l'erreur
      //int d = e;     // Déclaration de d et d <- e qui n'est pas encore déclarée et donc non visible.
      //int e = 5;     // Déclaration et initialisation de e.

      /*
         Déclaration de initialisation des constantes

         final <type> <identifier> [= expression]

         Comme son nom l'indique, il n'est pas possible de la modifier une
         fois initialisée.

         Concernant les conventions de nommage, une constante est toujours écrite
         en majuscule. Lorsqu'elle est composée de plusieurs mots, ceux-ci sont
         séparés par un _.
       */
      final int X;   // Déclaration d'une constante nommée X.
      X = 3;         // Initialisation : X <- 3.
      final int LA_REPONSE_A_TOUTES_LES_QUESTIONS = 42;
      System.out.println("Valeur de la constante X = " + X);
      System.out.println("La réponse à toutes les questions : " +
                         LA_REPONSE_A_TOUTES_LES_QUESTIONS);

      //Décommenter la prochaine ligne pour tester l'erreur
      //X = 2;         // La variable a déjà été initialisée, elle ne peut plus l'être.

      /*
         Valeurs initiales des variables et constantes.
       */
      valeursInitiales();

      /*
         Notions d'opération.
       */
      operations1();

      /*
         Déclaration avec opérations
       */
   }

   private static void valeursInitiales() {

   }

   private static void operations1() {
      SODisplay.printTitle2("Opérations");
      int a = 4 + 5;
      int b = 4/2/2;
      System.out.println(b);

      System.out.println("\'a\' + \'b\' = " + (char) ('a' + 'b'));
      System.out.println((int) 'a');
      System.out.println((int) 'b');
      System.out.println((int) ('a' + 'b'));
      String str1 = "Bon";
      str1 += "jour";
      String str2 = "Bonjour";
      String str3 = "Bonjour";
      String str7 = "Bon" + "jour";
      System.out.println(str1 == str2);
      System.out.println(str2 == str3);
      System.out.println(str3 == str7);

      String str4 = new String("Bonjour");
      String str5 = new String("Bonjour");
      System.out.println(str4 == str5);
      System.out.println(str4.equals(str5));
      Square sqr1 = new Square(5);
      Square sqr2 = new Square(5);
      System.out.println(sqr1 == sqr2);
      System.out.println(sqr1.equals(sqr2));

      int tern1 = (5 > 4)? 3: 2;
      System.out.println("tern1 " + tern1);
      int tern2;
      if(5>4) {
         tern2 = 3;
      } else {
         tern2 = 2;
      }
      System.out.println("tern2 " + tern2);

      int maVariable = 5;
      System.out.println(Math.sqrt(maVariable));
      int result = (int) Math.sqrt(maVariable);
      char intChar = ('a' + 4);
      System.out.println(intChar);

      char[] test = {'h', 'e', 'l', 'l', 'o'};
      for(int i = 0; i < test.length; i++) {
         test[i] = (char) (test[i] + 4);
      }
      ArrayUtil.printArray(test);
      String str10 = new String(test);

   }

   public static void conventionDeNomage() {

   }

   public static void bloc() {
      int b;
      {
         int a = 5;
         b = a;
         {
            System.out.println(a);
         }
      }
      //System.out.println(a);
      System.out.println(b);
   }

   public static void portee() {

   }

   private static void operations2() {

   }

   private static void allocationMemoire() {
            /*
         Lorsqu'une variable de type primitive est passée en paramètre à une
         méthode, c'est une copie de celle-ci que l'appel de la méthode peut
         modifier et pas l'originale.
       */
      SODisplay.printTitle1("Modification de variable par appel à une méthode");
      SODisplay.printTitle2("Modification d'une variable de type primitif");
      int nombre = 0;
      int nbRetour = 0;
      System.out.println("[out - avant modif] nombre = " + nombre +
              "; nbRetour = " + nbRetour);
      nbRetour = modifInt(nombre);
      // On voit bien ici que la variable n'a pas été changé malgré un changement
      // en interne.
      System.out.println("[out - après modif] nombre = " + nombre +
              "; nbRetour = " + nbRetour);


      /*
         Lorsqu'on passe une variable de type référence en paramètre à une méthode,
         une modification de cette variable est visible hors de la méthode.
         Cependant, si la variable est de nouveau initialisé, ce changement n'est
         pas visible en dehors de l'appel à la méthode.
       */
      SODisplay.printTitle2("Modification d'une variable de référence (tableau)");
      int[] tab = {1, 2, 3, 4};
      SODisplay.printTitle3("[out - av appel] tab");
      System.out.println(tab);
      SODisplay.printTitle3("[out - av appel] éléments tab");
      ArrayUtil.printArray(tab);
      modifArray(tab, 1, -42);
      SODisplay.printTitle3("[out - ap appel] tab");
      // On voit bien que la référence de tab n'a pas changée.
      System.out.println(tab);
      SODisplay.printTitle3("[out - ap appel] éléments de tab:");
      // Suite à la modification tab[1] <- -42, le deuxième élément est modifié.
      ArrayUtil.printArray(tab);

      // Les chaînes de caractères peuvent avoir un comportement particulier.
      declarationDeString();
   }

   public static void declarationDeString() {

   }

   private static int modifInt(int nombre) {
      System.out.println("[in - avant modif] nombre = " + nombre);
      nombre = 42;
      System.out.println("[in - après modif] nombre = " + nombre);

      return nombre;
   }

   private static void modifArray(int[] tab, int indice, int value) {
      SODisplay.printTitle3("[in - av modif] tab");
      System.out.println(tab);
      tab[indice] = value;
      SODisplay.printTitle3("[in - ap modif] éléments de tab");
      ArrayUtil.printArray(tab);
      tab = new int[]{5, 6};
      SODisplay.printTitle3("[in - ap nouvelle init.] tab");
      // On peut remarquer ici que la référence a été modifié.
      System.out.println(tab);
      SODisplay.printTitle3("[in - ap nouvelle init.] éléments de tab");
      ArrayUtil.printArray(tab);
   }
}